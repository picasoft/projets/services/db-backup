#!/usr/bin/env bash
# Antoine Barbare 18/02/18 <antoine@barbare.me>
# Quentin Duchemin 20/10/19 add --forceTableScan to prevent error on newest Mongo versions

BACKUP_FOLDER=${BACKUP_FOLDER:-"/backup/"}

[ -z "${MONGO_HOST}" ] && { echo "=> MONGO_HOST cannot be empty" && exit 1; }
[ -z "${MONGO_PORT}" ] && { echo "=> MONGO_PORT cannot be empty" && exit 1; }
[ -z "${MONGO_DB}" ] && { echo "=> MONGO_DB cannot be empty" && exit 1; }

ping -c 1 -W 1 "${MONGO_HOST}" || { echo -e "\n=========== ${MONGO_HOST} not available, skipping backup... ===========\n"; exit 1; }

if [[ -z "${MONGO_USER}" && -z "${MONGO_PASS}" ]]; then
  BACKUP_CMD="mongodump --forceTableScan -d ${MONGO_DB} -h ${MONGO_HOST}:${MONGO_PORT} -o $BACKUP_FOLDER"'${BACKUP_NAME}'
  RESTORE_CMD="mongorestore -d ${MONGO_DB} -h ${MONGO_HOST}:${MONGO_PORT} "
else
  BACKUP_CMD="mongodump --forceTableScan -d ${MONGO_DB} -h ${MONGO_HOST}:${MONGO_PORT} -u ${MONGO_USER} -p ${MONGO_PASS} -o $BACKUP_FOLDER"'${BACKUP_NAME}'
  RESTORE_CMD="mongorestore -d ${MONGO_DB} -h ${MONGO_HOST}:${MONGO_PORT} -u ${MONGO_USER} -p ${MONGO_PASS} "
fi

# Make sure backup folder exists
mkdir -p "${BACKUP_FOLDER}"
##########################
# CREATING BACKUP SCRIPT #
##########################
backup_script_name="${MONGO_SERVICE_NAME}-backup.sh"

echo "=> ${MONGO_SERVICE_NAME}: Creating backup script"
rm -f "/$backup_script_name"

cat <<EOF >> "/$backup_script_name"
#!/bin/bash

BACKUP_NAME=\$(date +\%Y.\%m.\%d.\%H\%M\%S)

echo "=> ${MONGO_SERVICE_NAME}: Backup started: \${BACKUP_NAME}"
if ${BACKUP_CMD} ;then
    MONGO_BACKUP=$BACKUP_FOLDER\${BACKUP_NAME}
    echo " => Compress files \$MONGO_BACKUP.tar.gz"
    tar -czf \$MONGO_BACKUP.tar.gz \$MONGO_BACKUP && \
    rm -rf \$MONGO_BACKUP && \
    echo "${MONGO_SERVICE_NAME}:  Backup succeeded"
else
    echo "${MONGO_SERVICE_NAME}: Backup failed"
    rm -rf $BACKUP_FOLDER\${BACKUP_NAME}
fi
EOF
chmod +x /$backup_script_name

###########################
# CREATING RESTORE SCRIPT #
###########################
restore_script_name="${MONGO_SERVICE_NAME}-restore.sh"
backup_log="${MONGO_SERVICE_NAME}_mongo_backup.log"

echo "=> ${MONGO_SERVICE_NAME}: Creating restore script"
rm -f "/$restore_script_name"

cat <<EOF >> /$restore_script_name
#!/bin/bash

echo "=> ${MONGO_SERVICE_NAME}: Restore database from \$1"
echo "  => Uncompress save \$1"
tar -xzvf \$1
output="\$(echo \$1 | awk -F'.tar.gz' '{print \$1}')"
if mongorestore -d ${MONGO_SERVICE_NAME} -h ${MONGO_HOST}:${MONGO_PORT} \$output;then
      echo "${MONGO_SERVICE_NAME}: Restore succeeded"
else
      echo "${MONGO_SERVICE_NAME}: Restore failed"
fi
rm -Rf \$output
EOF
chmod +x /$restore_script_name

touch /$backup_log
tail -F /$backup_log &

if [ -n "${INIT_BACKUP}" ]; then
    echo "=> ${MONGO_SERVICE_NAME}: Create a backup on the startup"
    /$backup_script_name
elif [ -n "${INIT_RESTORE_LATEST}" ]; then
    echo "=> ${MONGO_SERVICE_NAME}: Restore latest backup"
    until nc -z $MONGO_HOST $MONGO_PORT
    do
        echo "waiting database container..."
        sleep 1
    done
    ls -d -1 $BACKUP_FOLDER* | tail -1 | xargs /$restore_script_name
fi

echo "${CRON_TIME} /$backup_script_name >> /$backup_log 2>&1" >> /crontab.conf
crontab  /crontab.conf
echo "=> ${MONGO_SERVICE_NAME}: Running cron job"
