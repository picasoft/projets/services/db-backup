FROM debian:sid-slim

RUN apt-get update && \
    apt-get install -y --no-install-recommends \
    iputils-ping \
    postgresql-client-12 \
    python3 \
    default-mysql-client \
    mongo-tools \
    cron && \
    apt-get clean && \
    mkdir -p /scripts/config

COPY backup_env_var.py run.sh /scripts/
COPY mysql-run.sh /scripts/mysql/mysql-run.sh
COPY postgres-run.sh /scripts/postgresql/postgres-run.sh
COPY mongo-run.sh /scripts/mongodb/mongo-run.sh

ENV CRON_TIME="0 3 * * *"

VOLUME ["/backup"]

CMD ["/scripts/backup_env_var.py"]
